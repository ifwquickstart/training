-- hl7.findSegment is installed directly into the hl7 namespace
require 'hl7util'
require 'dateparse'
-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
function main(Data)
   --parse the HL7 Message
   local Orig, Name, Error = hl7.parse{vmd='demo.vmd',data = Data}
   --create an outbound message
   local Out = hl7.message{vmd='demo.vmd', name=Orig:nodeName()}
   --Map original incoming to outbound message
   Out:mapTree(Orig)
   --[[Create a variable that will be used to check 
   if messages segments exists(see function below)]]
  local PID = hl7util.findSegment(Orig, FindPID)
  local OBX = hl7util.findSegment(Orig, FindOBX)
   --Altering MSH
   Out.MSH[3][1] = 'MyApp'
   Out.MSH[4][1] = 'MyOffice'
   if Orig.MSH[9][1]:nodeValue() == 'ADT'then
      Out.MSH[9][1] = 'Admission Discharge Transfer'
   elseif Orig.MSH[9][1]:nodeValue() == 'LAB' then
      Out.MSH[9][1] = 'Laboratory'
   else
       Out.MSH[9][1] =  Out.MSH[9][1]
   end
   --Checking if PID segment exists, if yes, then alter the PID
   if PID then
   Out.PID[5][1][2] = Out.PID[5][1][2]:nodeValue():upper()
   Out.PID[5][1][1][1] = Out.PID[5][1][1][1]:nodeValue():upper()
   local DOB = dateparse.parse(Out.PID[7][1]:nodeValue())
   Out.PID[7][1] = os.date('%Y%m%d', DOB)
   end
   --Checking if OBX segment exists, if yes, it Alters OBX segments
   if OBX then
      for i=1, #OBX do
         trace(Orig.OBX[i][5]:S())
         if Orig.OBX[i][3][1]:nodeValue() == 'WT' and Orig.OBX[i][6][1]:S() == 'pounds' then 
            Out.OBX[i][5][1][1]:setNodeValue(tonumber(Orig.OBX[i][5]:S()) * 0.454)
            Out.OBX[i][6][1] = 'Kg'
         end
      end  
   end
   --Check the errors in the message and send a message to the logs
   if Error[1]~= nil then
      for k, v in pairs(Error) do 
         if Error[1].important == true then
            Error[1].description = 'Important Error, '..Error[1].description
            iguana.logInfo(Error[1].description)
            break
         else
            Error[1].description = 'Minor Error, '..Error[1].description
            iguana.logInfo(Error[1].description)
            break
         end
      end
   end
   
   if Error[2]~= nil then
      for k, v in pairs(Error) do 
         if Error[2].important == true then
            Error[2].description = 'Important Error, '..Error[2].description
            iguana.logInfo(Error[2].description)
            break
         else
            Error[1].description = 'Minor Error, '..Error[2].description
            iguana.logInfo(Error[2].description)
            break
         end
      end
   end
   
    if Error[3]~= nil then
      for k, v in pairs(Error) do 
         if Error[3].important == true then
            Error[3].description = 'Important Error, '..Error[3].description
            iguana.logInfo(Error[3].description)
            break
         else
            Error[3].description = 'Minor Error, '..Error[3].description
            iguana.logInfo(Error[3].description)
            break
         end
      end
   end
  
    --push data to queue
   local DataOut = tostring(Out)
   queue.push(DataOut)
   
end

-- function to find (match) a PID segment
function FindPID(Segment)
   if Segment:nodeName() == 'PID' then
      return true
   end
end


-- function to find (match) a PID segment
function FindOBX(Segment)
   if Segment:nodeName() == 'OBX' then
      return true
   end
end