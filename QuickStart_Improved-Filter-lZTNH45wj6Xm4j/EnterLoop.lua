function Er(Error)
   -- Log all validation errors on incoming message.
   for i= 1, #Error do 
      for k, v in pairs(Error) do 
         if Error[i].important == true then
            Error[i].description = 'Important Error, '..Error[i].description
            iguana.logInfo(Error[i].description)
            break
         else
            Error[i].description = 'Minor Error, '..Error[i].description
            iguana.logInfo(Error[i].description)
            break
         end
      end
   end
end