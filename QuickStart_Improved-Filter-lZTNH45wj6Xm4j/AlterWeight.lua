--Checking if OBX segment exists, if yes, it Alters OBX segments
function AlterWeight(OBX)   
   if OBX then
      for i=1, #OBX do
         trace(OBX[i][5]:S())
         if OBX[i][3][1]:nodeValue() == 'WT' and OBX[i][6][1]:S() == 'pounds' then 
            OBX[i][5][1][1]:setNodeValue(tonumber(OBX[i][5]:S()) * 0.454)
            OBX[i][6][1] = 'Kg'
         end
      end  
   end
end