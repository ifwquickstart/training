require 'dateparse'
require 'AlterMSH'
require 'AlterPID'
require 'AlterWeight'
require 'EnterLoop'

-- The main function is the first function called from Iguana.
-- The Data argument will contain the message to be processed.
function main(Data)
   --Parse the HL7 Message
   local Orig, Name, Error = hl7.parse{vmd='demo.vmd',data = Data}
   
   --Create an outbound message
   local Out = hl7.message{vmd='demo.vmd', name=Orig:nodeName()}
   
   --Map original incoming to outbound message
   Out:mapTree(Orig)
   
   --Change information in the message header
   AlterMSH(Out.MSH)
   
   --Change the information in message header
   AlterPID(Out.PID)
   
   --Change the weight measuring scale
   AlterWeight(Out.OBX)
   
   --Change error messages and trace the values
   Er(Error)
   
   --push data to queue
   trace(Out)
   local DataOut = tostring(Out)
   queue.push(DataOut)
   

end