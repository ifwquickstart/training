--Checking if PID segment exists, if yes, then alter the PID
function AlterPID(PID)    
   if PID then
      PID[5][1][2] = PID[5][1][2]:nodeValue():upper()
      PID[5][1][1][1] = PID[5][1][1][1]:nodeValue():upper()
      local DOB = dateparse.parse(PID[7][1]:nodeValue())
      PID[7][1] = os.date('%Y%m%d', DOB)
   end
end